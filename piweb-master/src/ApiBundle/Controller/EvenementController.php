<?php
/**
 * Created by PhpStorm.
 * User: Mechlaoui
 * Date: 26/04/2018
 * Time: 20:21
 */

namespace ApiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use UtilisateurBundle\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class EvenementController extends Controller
{
    public function loginAction(Request $request)
    {
        $username = $request->query->get("username");
        $password = $request->query->get("password");
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UtilisateurBundle:User")->findOneBy(['username' => $username]);
        $user->setPlainPassword($user->getPlainPassword());
        if($user) {
            if (password_verify($password, $user->getPassword())) {
                $serializer = new Serializer([new ObjectNormalizer()]);
                $formatted = $serializer->normalize($user);
                return new JsonResponse($formatted);
            } else {
                return new Response("failed");
            }
        }
        else{
            return new Response("failed");
        }

    }

    public function registerAction(Request $request){

        $username = $request->query->get("username");
        $password = $request->query->get("password");
        $email = $request->query->get("email");
        $niveau = $request->query->get("niveau");

        $user = new User();
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEmail($email);
        $user->setNiveau($niveau);

        try {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return new Response("success");

        }
        catch (Exception $ex){
            return new Response("fail");
        }
    }

    public function findWeekAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $events = $em->getRepository('UtilisateurBundle:Evenement')->findWeek();
        if($events) {
            $encoder = new JsonEncoder();
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceHandler(function($object){
                return $object->getId();
            });
            $serializer = new Serializer([$normalizer], [$encoder]);
            $formatted = $serializer->normalize($events);
            return new JsonResponse($formatted);
        }
        else{
            return new Response("no data");
        }
    }

    public function findAllAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $events = $em->getRepository('UtilisateurBundle:Evenement')->findAll();
        foreach ($events as $event){
            $dispo = $event->getNombre() - count($em->getRepository('UtilisateurBundle:Reservation')->findBy(["idEvenement" => $event, "etat" => "Confirmé"]));
            $event->setDisponible($dispo);
        }

        if($events) {
            $encoder = new JsonEncoder();
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceHandler(function($object){
                return $object->getId();
            });
            $serializer = new Serializer([$normalizer], [$encoder]);
            $formatted = $serializer->normalize($events);
            return new JsonResponse($formatted);
        }
        else{
            return new Response("no data");
        }
    }

    public function Images1Action(Request $request){
        $image = $request->query->get("img");
        return $this->render('ApiBundle:Images:images.html.twig',["image" => $image]);
    }

    public function Images2Action(Request $request){
        $image = $request->query->get("img");
        $path = '\web\affiches\\'.$image;
        $file =    readfile(__DIR__.'/../../../'.$path);
        $headers = array(
            'Content-Type'     => 'image/jpg',
            'Content-Disposition' => 'inline; filename="'.$image.'"');
        return new Response($file, 200, $headers);
    }
    public function ImagesAction(Request $request)
    {
        $publicResourcesFolderPath = $this->get('kernel')->getRootDir() . '/../web/affiches/';
        $image = $request->query->get("img");

        // This should return the file located in /mySymfonyProject/web/public-resources/TextFile.txt
        // to being viewed in the Browser
        return new BinaryFileResponse($publicResourcesFolderPath.$image);
    }


    public function EditUserAction(Request $request){

    }

    public function EventByIdAction(Request $request){
        $id = $request->query->get("id");
        $em = $this->getDoctrine()->getEntityManager();
        $events = $em->getRepository('UtilisateurBundle:Evenement')->find($id);
        $dispo = $events->getNombre() - count($em->getRepository('UtilisateurBundle:Reservation')->findBy(["idEvenement" => $events, "etat" => "Confirmé"]));
        $events->setDisponible($dispo);

        if($events) {
            $encoder = new JsonEncoder();
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceHandler(function($object){
                return $object->getId();
            });
            $serializer = new Serializer([$normalizer], [$encoder]);
            $formatted = $serializer->normalize($events);
            return new JsonResponse($formatted);
        }
        else{
            return new Response("no data");
        }

    }
}